
var webdriver = require('selenium-webdriver'),
    chai = require('chai'),
    test = require('selenium-webdriver/testing'),
    expect = chai.expect,
    basePage = require('../pages/base_page'),
    homePage = require('../pages/home_page'),
    resultsPage = require('../pages/results_page');

test.describe('Search for something', function() {
    this.timeout(15000);

    test.before(function() {
        driver = new webdriver.Builder().
            withCapabilities(webdriver.Capabilities.chrome()).
            build();
        
        homePage.driver = driver;
        basePage.driver = driver;
        resultsPage.driver = driver;
    });

    test.beforeEach(function() {
       homePage.goto();
    });

    test.it('Results are returned', function () {
        basePage.wait_for_page(2000)
            .then(homePage.search_field().sendKeys('90210'))
            .then(homePage.search_button().click())
            .then(basePage.wait_for_page(5000))
            .then(resultsPage.results_count)
            .then(function(results) {
                expect(results).to.not.equal('0 Homes');
            })
    });

    test.after(function() {
        driver.quit();
    });
});
