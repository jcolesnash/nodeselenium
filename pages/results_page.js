var webdriver = require('selenium-webdriver'),
    By = webdriver.By;

module.exports = {
    results_count: function() {
        return this.driver.findElement(By.id('search-result-count')).getText();
    },
};