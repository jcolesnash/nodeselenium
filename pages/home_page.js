var webdriver = require('selenium-webdriver'),
    By = webdriver.By;

module.exports = {
    goto: function() {
        this.driver.get('http://www.realtor.com/');
    },
    search_field: function() {
        return this.driver.findElement(By.name('q'));
    },
    search_button: function() {
        return this.driver.findElement(By.css('.home-header-content .search-input-prepopulate .js-searchButton'));
    },
};