var webdriver = require('selenium-webdriver');

module.exports = {
    wait_for_page: function(milliseconds) {
        return this.driver.sleep(milliseconds);
    },
};